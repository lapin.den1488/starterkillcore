using System.Threading.Tasks;
using HackatonApiCoreNethereum.Attributes;
using HackatonApiCoreNethereum.Helpers;
using HackatonApiCoreNethereum.Models.Enums;
using HackatonApiCoreNethereum.Models.ViewModels.User;
using HackatonApiCoreNethereum.Services;
using HackatonApiCoreNethereum.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace HackatonApiCoreNethereum.Controllers
{

    public class AccountController : Controller
    {

        private readonly IUserService userService;

        public AccountController(IUserService userService)
        {
            this.userService = userService;
        }

        [HttpGet("/Account/GetInfo")]
        [EnumRole(Role.User, AuthenticationSchemes = "Bearer")]
        public async Task<IActionResult> GetInfo()
        {
            var result = await userService.GetInfoAsync(User.Identity.Name);
            if (result.Succeeded)
                return Ok(result.Object);
            else
                AddError(result);

            return BadRequest(ModelState);
        }

        [HttpPost("/Account/Create")]
        [AllowAnonymous]
        public async Task<IActionResult> Create([FromForm]CreateUserViewModel model)
        {
            if (ModelState.IsValid)
            {
                var result = await userService.CreateAsync(model);
                if (result.Succeeded)
                    return Ok(model);
                else
                    AddError(result);
            }

            return BadRequest(ModelState);
        }

        [HttpPut("/Account/Edit")]
        [EnumRole(Role.User, AuthenticationSchemes = "Bearer")]
        public async Task<IActionResult> Edit([FromForm]EditUserViewModel model)
        {
            if (ModelState.IsValid)
            {
                var result = await userService.EditAsync(User.Identity.Name, model);
                if (result.Succeeded)
                    return Ok(model);
                else
                    AddError(result);
            }

            return BadRequest(ModelState);
        }

        [HttpDelete("/Account/Delete")]
        [EnumRole(Role.User, AuthenticationSchemes = "Bearer")]
        public async Task<IActionResult> Delete()
        {
            var user = User.Identity.Name;
            var result = await userService.DeleteAsync(User.Identity.Name);
            if (result.Succeeded)
                return Ok();
            else
                AddError(result);

            return BadRequest(ModelState);
        }

        /*[HttpGet("/Account/Confirm")]
        [AllowAnonymous]
        public async Task<IActionResult> EmailConfirm([FromQuery]string id,[FromQuery]string token){
            var result = await userService.ConfirmMail(id,token);
            if(result.Succeeded)
                return Ok();
            else
                AddError(result);
            
            return BadRequest(ModelState);
        }
        */
        [NonAction]
        public void AddError(IdentityResult result)
        {
            foreach (var item in result.Errors)
            {
                ModelState.AddModelError("", item.Description);
            }
        }
    }
}