using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace HackatonApiCoreNethereum.Models.ViewModels.User{
    public class CreateUserViewModel{
        
        [Required]
        [Display(Name = "Username")]
        public string UserName { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
        
        [Required]        
        [Display(Name = "Public Address")]
        public string PublicAddress{get;set;}
        
        [Required]
        [Display(Name = "Password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        
        [Required]
        [Display(Name = "Confirm password")]
        [DataType(DataType.Password)]
        [Compare("Password")]
        public string Confirmpass { get; set; }

        [DataType(DataType.Upload)]
        [Display(Name = "Profile Image")]
        public IFormFile ProfileImage {get;set;}
    }
}