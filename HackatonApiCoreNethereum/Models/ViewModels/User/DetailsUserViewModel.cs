using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace HackatonApiCoreNethereum.Models.ViewModels.User{
    public class DetailsUserViewModel{
        
        
        [Display(Name = "Username")]
        public string UserName { get; set; }
        
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }        
               
        [Display(Name = "Public Address")]
        public string PublicAddress{get;set;}

        [Display(Name = "Argon Balance")]
        public int ArgonBalance{get;set;}

        [Display(Name = "Phone Number")]
        public string PhoneNumber{get;set;}        

        public string ProfileImagePath { get; set; }
    }
}